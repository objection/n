# N

## Well, well, well, what's this all about?

This is N, so called because it's about N THINGS. N cigarettes, N
chocolate biscuits. I'm giving up smoking.

The idea is you tell the program every time you do a THING, and the
next time you come to do that THING, you ask the program to remind you
how many of that THING you have done that day.
And it tells you, like this:

```
4/5
```

The five is MAX, the MAX number of times you have allowed yourself to
do the THING.

It's like making a mark on a piece of paper, except much, much better.

Now, what will happen if you go over MAX? If you are so weak as to
smoke six cigarettes instead of five?

Something terrible: you'll disappoint yourself. You'll see that
accusing 6/5, and feel bad. You'll always feel bad. Even in thirty
years, at the graduation of your first-born child, you'll feel bad.

"Thank you, Daddy. You're the best daddy in the whole wide world!"

And you smile. Thank God your child is too self-absorbed to see the
pain in your eyes, and ...

6/5.
6/5.

## Install

CD to where you want to put the source code. You need Meson, a build
system you can get from your package manager.

```
git clone --recursive 'https://gitlab.com/objection/n.git'
meson build
ninja -Cbuild
```

If you want to install it so you can use it from anywhere in your
shell, do `ninja -Cbuild install`.

## How to use

Step 1: add a THING:

```
n --new='name=cigarettes,max=5'
```

Then, smoke a cigarette. Then type:

```
n cigarettes --increment
```

Then type:

```
n cigarettes
```

And behold, the program agrees with you. It says, "1/5".

Next, take control of your coffee habit. You have to drink a cup of
coffee first, paradoxically.

```
n -n='name=coffee,max=3'
```

You've already drunk 2 cups of coffee, so use --set:

```
n --set=2
```

And so on. It's all the stuff you'd expect. You can remove an entry
with --remove, you can set it to 0 with --reset. Decrement with
--decrement. All of these options have short options.

You can use shell globbing:

```
n \*
```

Will print:

```
cigarettes 4/5
coffee     3/4
drugs      2/4
```

