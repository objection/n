#include "lib/darr/darr.h"
#define MAX_NAME 32
// The basename of the file everything's recorded in.
// The full path'll be ~/.local/share/n.txt.
#define N_FILE_NAME "n.txt"
// Print the total even when you're incrementing or whatever.
#define ALWAYS_PRINT 1
// Print the name, even when it's the only match.
#define ALWAYS_PRINT_NAME 0
// Use globbing instead of regex. You're better using regex, cause then
// you can just type cig to match cigarettes. With fnmatch you need to cig\*
#define USE_GLOB 0
// The format you want your dates to be printed out in. Check strftime
// manpage ("man strftime" in terminal or google) for list of the various
// %Xes.
#define DATE_FMT_STRING "%H:%M:%S"
// For --next. Which hour you consider the day to end, an int.
// Eg, if you go to bed at 2am, write two. If you go to bed at 2pm, write
// 14.
// I feel it incumbent upon me to point out that I could have you use a
// proper time here, like 02:00:00. But I can't be bothered. And who cares?
#define END_OF_DAY 2
char *progname;
