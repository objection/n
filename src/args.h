#include <stdbool.h>
enum operate_on {
	OPERATE_ON_N,
	OPERATE_ON_MAX,
};
enum action {
	ACTION_DEFAULT_PRINT,
	ACTION_DECREMENT,
	ACTION_INCREMENT,
	ACTION_NEW,
	ACTION_REMOVE,
	ACTION_RESET,
	ACTION_SET,
};

struct args {
	char *pattern;
	enum action action;
	struct entry *new;
	enum operate_on operate_on;
	char *n_file;
	int how_much; // If -i or -d has arg, this is where it goes
	bool print_next; // A modifier for print
};

struct args get_args (int argc, char **argv);
