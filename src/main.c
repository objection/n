#include <err.h>
#include "lib/darr/darr.h"
#include "lib/time_string/time_string.h"
#include "config.h"
#include "parse.h"
#include "args.h"
#include <string.h>
#if USE_GLOB
#include <fnmatch.h>
#else
#include <sys/types.h>
#include <regex.h>
#endif
#include <argp.h>
#include <stdlib.h>
#include <assert.h>

DEFINE_ARR (char, char);
DEFINE_ARR (int, int);

#define N_SCRATCH 1024
char scratch[N_SCRATCH];

bool regmatch (const char *str, const char *pattern) {
	regex_t regex;
	int result;
	if ((result = regcomp (&regex, pattern, REG_EXTENDED | REG_NOSUB))) {
		char msgbuf[126];
		regerror (result, &regex, msgbuf, sizeof msgbuf);
		errx (1, "regcomp failed: %s\n", msgbuf);
	};
	result = regexec (&regex, str, 0, NULL, 0);
	regfree (&regex);
	if (!result) return 1;
	return 0;
}

struct int_arr get_entries (char *pattern, struct entry_arr entries) {
	struct int_arr res = create_int_arr (8);
	for (struct entry *entry = entries.d;
			entry - entries.d < entries.n; entry++) {
#if USE_GLOB
		if (!fnmatch (pattern, (*entry).name, 0)) {
			push_int_arr (&res, entry - entries.d);
		}
#else
		if (regmatch ((*entry).name, pattern)) {
			push_int_arr (&res, entry - entries.d);
		}
#endif
	}
	return res;
}

static void write_entries (struct entry_arr entries, char *n_file) {
	FILE *fp = fopen (n_file, "w");
	if (!fp) errx (1, "Couldn't open %s", n_file);

	for (struct entry *entry = entries.d;
			entry - entries.d < entries.n; entry++) {
		if (!(*entry).remove) {
			fprintf (fp, "%s %d/%d\n", (*entry).name, (*entry).n,
					(*entry).max_allowed);
		}
	}
	fprintf (fp, "\n");
	fclose (fp);
}


static void print (struct entry *entry, bool is_multiple, int max_n_name,
		bool print_next) {

	if (is_multiple || ALWAYS_PRINT_NAME) {
		printf ("%s ", (*entry).name);
		if (is_multiple) {
			int n_name = strlen ((*entry).name);
			for (int i = n_name; i < max_n_name; i++) {
				putchar (' ');
			}
		}
	}
	printf ("%d/%d", (*entry).n, (*entry).max_allowed);

	if (print_next) {

		time_t now = time (NULL);

		struct tm set_to;
		set_to.tm_year =
			set_to.tm_mon = -1;
		set_to.tm_sec = set_to.tm_min = 0;
		set_to.tm_hour = END_OF_DAY;

		time_t end_of_day = ts_timestamp_with_units_set (now +
				TS_SECS_FROM_DAYS (1), &set_to);

		time_t time_till_end_of_day = end_of_day - now;

		printf ("    ");
		if ((*entry).n > (*entry).max_allowed) {
			printf ("!");
		} else if ((*entry).n == (*entry).max_allowed) {
			printf (".");
		} else {
			// Note it's (max - n + 1). 
			ts_string_from_timestamp (scratch, N_SCRATCH, now +
					(time_till_end_of_day / ((*entry).max_allowed - (*entry).n
											 + 1)), DATE_FMT_STRING);
			printf ("%s", scratch);
		}
	}
	putchar ('\n');
}

int main (int argc, char **argv) {
	progname = argv[0];

	struct args args = get_args (argc, argv);

	int max_n_name = 0;
	struct entry_arr entries = parse_n_file (args.n_file, &max_n_name);

	struct int_arr list;
	if (args.action != ACTION_NEW) {
		if (!entries.n) {
			errx (1, "\
%s is empty: put something in it with \"n --new=name=<name>,max=<max>\"",
	args.n_file);
		}
		if (!args.pattern) {
#if USE_GLOB
			args.pattern = "*";
#else
			args.pattern = ".*";
#endif
		}
		list = get_entries (args.pattern, entries);
		if (!list.n) errx (1, "There's nothing matching \"%s\" in %s",
				args.pattern, args.n_file);
	}

	if (args.action == ACTION_NEW) {
		push_entry_arr (&entries, *args.new);
	} else {

		for (int i = 0; i < list.n; i++) {

			struct entry *entry = entries.d + list.d[i];
			int *operate_on_val =
				args.operate_on == OPERATE_ON_MAX?
					&(*entry).max_allowed:
					&(*entry).n;

			switch (args.action) {
				case ACTION_DECREMENT:
					(*operate_on_val) -= args.how_much? args.how_much: 1;
					break;
				case ACTION_INCREMENT:
					(*operate_on_val) += args.how_much? args.how_much: 1;
					break;
				case ACTION_REMOVE:
					(*entry).remove = 1;
					break;
				case ACTION_RESET:
					*operate_on_val = 0;
					break;
				case ACTION_SET:
					*operate_on_val = (*args.new).n;
					break;
				default:
					print (entry, list.n > 1, max_n_name, args.print_next);
					break;
			}

			if (args.action != ACTION_REMOVE
					&& (args.action != ACTION_DEFAULT_PRINT
						&& ALWAYS_PRINT)) {
				print (entry, list.n > 1, max_n_name, args.print_next);
			}
		}
	}

	if (args.action != ACTION_DEFAULT_PRINT) {
		write_entries (entries, args.n_file);
	}

	exit (0);
}
