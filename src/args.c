#define _GNU_SOURCE
#include "args.h"
#include "parse.h"
#include "config.h"
#include <argp.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <err.h>

enum long_option {
	LONG_OPTION_REMOVE = 500,
};

enum got {
	GOT_ACTION = 1 << 0,
	GOT_NAME = 1 << 1,
} got;

enum name_vals {
	NAME_VALS_NAME,
	NAME_VALS_MAX,
};

char *name_vals_strs[] = {
	[NAME_VALS_NAME] = "name",
	[NAME_VALS_MAX] = "max",
	NULL,
};

static int get_int (int *res, char *str) {
	char *end;
	errno = 0;
	*res = strtol (str, &end, 0);
	if (errno || *end != '\0') return 1;
	return 0;
}

#define SET_ACTION(flag) \
	do { \
		if (got) errx (1, "Only one action allowed"); \
		(*args).action = flag; \
	} while (0);

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	struct args *args = (*state).input;
	switch (key) {
		case 'd':
			SET_ACTION (ACTION_DECREMENT);
			if (arg) {
				if (get_int (&(*args).how_much, arg)) {
					err (1, "Bad int");
				}
			}
			break;
		case 'i':
			SET_ACTION (ACTION_INCREMENT);
			if (arg) {
				if (get_int (&(*args).how_much, arg)) {
					err (1, "Bad int");
				}
			}
			break;
		case 'f':
			(*args).n_file = arg;
			break;
		case 'm':
			(*args).operate_on = OPERATE_ON_MAX;
			break;
		case 'n':
			SET_ACTION (ACTION_NEW);
			{
				char *val = NULL;
				int ret;
				bool got_name = 0, got_max = 0;
				while ((ret = getsubopt (&arg, name_vals_strs, &val)) != -1) {
					switch (ret) {
						case NAME_VALS_NAME:
							if (strlen (val) - 1 >= MAX_NAME) {
								errx (1, "Name too long");
							}
							strcpy ((*(*args).new).name, val);
							got_name = 1;
							break;
						case NAME_VALS_MAX:
							if (get_int (&(*(*args).new).max_allowed, val))
								errx (1, "%s is bad int", val);
							got_max = 1;
							break;
						default:
							break;
					}
				}
				if (!(got_name && got_max)) {
					errx (1, "\
You need to pass both name and value, like \"--new=name=<name>,max=<max>\"");
				}
			}
			break;
		case 'N':
			(*args).print_next = 1;
			break;
		case LONG_OPTION_REMOVE:
			SET_ACTION (ACTION_REMOVE);
			break;
		case 'r':
			SET_ACTION (ACTION_RESET);
			break;
		case 's':
			SET_ACTION (ACTION_SET);
			errno = 0;
			if (get_int (&(*(*args).new).n, arg))
				errx (1, "%s is bad int", arg);
			break;
		case ARGP_KEY_ARG:
			if (got & GOT_NAME) errx (1, "only one name allowed");
			(*args).pattern = arg;
			got |= GOT_NAME;
			break;
		default:
			break;
	}
	return 0;
}
#undef SET_ACTION

static char *get_n_file () {
	char *res = getenv ("HOME");
	asprintf (&res, "%s/.local/share/" N_FILE_NAME, res);
	return res;
}

struct args get_args (int argc, char **argv) {

	struct args res = {0};

	struct argp_option options[] = {
		{"decrement", 'd', "N", OPTION_ARG_OPTIONAL ,
			"Decrement your THING (optionally by N)"},
		{"increment", 'i', "N", OPTION_ARG_OPTIONAL,
			"Increment your THING (optionally by N)"},
		{"file", 'f', "FILE", 0, "Use FILE instead of the default"},
		{"remove", LONG_OPTION_REMOVE, NULL, 0, "Remove an THING"},
		{"reset", 'r', NULL, 0, "Set THING to 0"},
		{"set", 's', "VALUE", 0, "Set THING to VALUE"},
		{"new", 'n', "VALUE,MAX", 0, "Make a new entry, set its max"},
		{"next", 'N', NULL, 0, "Print when you can have the next THING"},
		{"max", 'm', NULL, 0, "Operate on MAX, rather than N"},
		{0},
	};

	res.new = malloc (sizeof *res.new);
	if (!res.new) assert (0);

	struct argp argp = {
		options, parse_opt,  "THING",
		"Count the number of bad THINGS you do",
	};

	argp_parse (&argp, argc, argv, 0, NULL, &res);

	if (!(res.n_file)) res.n_file = get_n_file ();

	return res;

}

