#include "config.h"
#include "lib/darr/darr.h"
#include <stdio.h>

struct entry {
	char name[MAX_NAME];
	int n;
	int max_allowed;
	bool remove;
};

DECLARE_ARR (struct entry, entry);

struct entry_arr parse_n_file (char *filename, int *max_n_name);

