#include "parse.h"
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <err.h>

#define MAX(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _a : _b; })


DEFINE_ARR (struct entry, entry);
#define PRINT_TOKEN 0

enum kind {
	KIND_NOT_SET,
	KIND_NAME,
	KIND_DIGITS,
	KIND_FORWARD_SLASH,
	KIND_NEWLINE,
	KIND_BAD_CHAR,
	KIND_END,
	N_KIND
};

char *kind_strs[] = {
	[KIND_NAME] = "name",
	[KIND_FORWARD_SLASH] = "/",
	[KIND_NEWLINE] = "newline",
	[KIND_DIGITS] = "digits",
	[KIND_BAD_CHAR] = "bad char",
	[KIND_END] = "end of file",
};

struct token {
	char *s, *e;
	enum kind kind;
	int line;
} t = {.line = 1}; // Count from one. I think Stallman told me to.

// Justification for these globals. They're not really globals,
// are they? They're local to this file. This file is my "class".
static char *end_of_parsing;
static char *g_filename;

static bool is_name_char (char p) {
	switch (p) {
		case 'a' ... 'z': 	// fall-through
		case 'A' ... 'Z': 	// fall-through
		case '_': 			// fall-through
		case '-': 			// fall-through
			return 1;
		default:
			return 0;
	}
}

static void get_token () {
	t.s = t.e;
	if (t.s >= end_of_parsing) {
		t.kind = KIND_END;
		return;
	}
	while (*t.s == ' ' || *t.s == '\t') t.s++;
	t.e = t.s + 1;
	switch (*t.s) {
		case '/':
			t.kind = KIND_FORWARD_SLASH;
			break;
		case '0' ... '9':
			while (isdigit (*t.e)) t.e++;
			t.kind = KIND_DIGITS;
			break;
		case '\n':
			t.line++;
			t.kind = KIND_NEWLINE;
			break;
		case 'a' ... 'z': 	// fall-through
		case 'A' ... 'Z': 	// fall-through
		case '_': 			// fall-through
		case '-': 			// fall-through
			while (is_name_char (*t.e)) t.e++;
			t.kind = KIND_NAME;
			break;
		default:
			if (t.s == end_of_parsing) t.kind = KIND_END;
			else t.kind = KIND_BAD_CHAR;
			break;
	}
}

static void print_token () {
#if PRINT_TOKEN
	printf ("%.*s, %s\n", (int) (t.e - t.s), t.s, kind_strs[t.kind]);
#endif
}

static void expect_token (enum kind kind) {
	get_token ();
	if (t.kind != kind) {
		errx (1, "%s:%d: was looking for %s but found %s",
				g_filename, t.line,
				kind_strs[kind], kind_strs[t.kind]);
	}
	print_token ();
}

static int get_int (int *res) {
	errno = 0;
	char *endptr;
	*res = strtol (t.s, &endptr, 0);
	if (errno) {
		assert (!"This shouldn't be able to fail");
		return 1;
	}
	if (endptr != t.e) assert (0);
	return 0;
}

static char *slurp (const char *path, ssize_t *n_buf) {
	errno = 0;

	FILE *fp = fopen (path, "re");
	if (fp == NULL) {
		return NULL;  	// use errno if returns NULL
	}

	size_t bufsiz = 0; 	// the size of the allocation in bytes
	errno = 0;
	char *res = NULL;
	*n_buf = getdelim (&res, &bufsiz, '\0', fp);
	if (res <= 0) {
		fclose (fp);
		return res; 	// use errno if returns NULL
	}
	fclose (fp);
	return res;
}

struct entry_arr parse_n_file (char *filename, int *max_n_name) {
	struct entry_arr res = create_entry_arr (8);
	g_filename = filename;
	ssize_t n_buf = 0;
	char *buf = slurp (filename, &n_buf);
	if (!buf) return res;

	end_of_parsing = buf + n_buf;
	t.e = buf;

	struct entry entry = {0};
	while (1) {
		while (1) {
			get_token ();
			if (t.kind == KIND_NAME) break;
			if (t.kind == KIND_END) goto out;
			if (t.kind != KIND_NEWLINE) {
				errx (1, "%s:%d: was looking for %s but found %s",
						g_filename, t.line,
						kind_strs[KIND_NAME], kind_strs[t.kind]);
			}
		}
		print_token ();
		if (t.e - t.s > MAX_NAME - 1) {
			errx (1, "%s:%d: name longer than MAX_NAME (%d)",
					g_filename, t.line, MAX_NAME);
		}

		*stpncpy (entry.name, t.s, t.e - t.s) = '\0';
		*max_n_name = MAX (t.e - t.s, *max_n_name);

		expect_token (KIND_DIGITS);
		get_int (&entry.n);
		expect_token (KIND_FORWARD_SLASH);
		expect_token (KIND_DIGITS);
		get_int (&entry.max_allowed);
		expect_token (KIND_NEWLINE);
		push_entry_arr (&res, entry);
	}
out:;
	free (buf);
	return res;
}

